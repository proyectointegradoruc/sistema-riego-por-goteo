﻿using MODELO.Interfaces;
using MODELO.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Guajira.Seguridad
{
    public class Autorizacion : AuthorizeAttribute
    {

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (string.IsNullOrEmpty(Sesion.Usuario))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Usuario", action = "IniciarSesion" }));
            }
            else
            {
                IUsuario modelo = new ModUsuario();
                DTO.Seguridad.ClsUsuario usuario = new DTO.Seguridad.ClsUsuario() { usuario = Sesion.Usuario, roles = Sesion.Roles };

                Principal p = new Principal(usuario);
                if (!p.IsInRole(Roles) && !string.IsNullOrEmpty(Roles))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "AccesoDenegado", action = "Index" }));
                }
            }
        }
    }
}