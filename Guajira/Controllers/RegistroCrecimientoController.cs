﻿using DTO.Rancheria;
using Guajira.Seguridad;
using Guajira.Utilidades;
using MODELO.Interfaces.Rancheria;
using MODELO.Modelos.Rancheria;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Guajira.Controllers
{
    [Autorizacion]

    public class RegistroCrecimientoController : Controller
    {
        IRegistroCrecimiento modelo = new ModRegistroCrecimiento();
        [HttpGet]
        public ActionResult Crear()
        {

            return View(new ClsRegistroCrecimiento());
        }
        [HttpPost]
        public ActionResult Crear(ClsRegistroCrecimiento model)
        {

            if (modelo.Crear(model))
            {
                ViewBag.Mensaje = Alertas.EXITO("Registro de crecimiento fue creado Satisfactoriamente");
            }
            else
            {
                ViewBag.Mensaje = Alertas.ERROR("No se pudo crear el registor del crecimiento,intentelo despues");

            }

            return View();
        }

        public ActionResult Listar()
        {

            List<ClsRegistroCrecimiento> crecimiento2 = modelo.ListarCrecimiento();
            List<ClsRegistroCrecimiento> crecimiento = new List<ClsRegistroCrecimiento>();

            if (crecimiento2 == null)
            {
                return View(crecimiento);
            }
            else
            {
                for (int i = 0; i < crecimiento2.Count; i++)
                {
                    if (crecimiento2[i].usuario.ToString().Equals(Sesion.Id))
                    {
                        crecimiento.Add(crecimiento2[i]);
                    }
                }

                if (Sesion.Id.Equals("1"))
                {
                    return View(crecimiento2);
                }
                else
                {
                    return View(crecimiento);
                }

            }

        }

        [HttpGet]
        public ActionResult Borrar(int id)
        {
            ClsRegistroCrecimiento r = new ClsRegistroCrecimiento();
            r.id = id;

            if (modelo.Borrar(r))
            {
                ViewBag.Mensaje = Alertas.EXITO("Registro del crecimiento Eliminado Satisfactoriamente");
            }
            else
            {
                ViewBag.Mensaje = Alertas.ERROR("No se pudo Eliminar el registro de crecimiento,intentelo despues");

            }

            return RedirectToAction("Listar");

        }
    }
}