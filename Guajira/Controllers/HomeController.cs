﻿using Guajira.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Guajira.Controllers
{
    public class HomeController : Controller
    {

        [Autorizacion]
        public ActionResult Index()
        {
            return View();
        }


        [Autorizacion(Roles = "EMPLEADO")]
        public ActionResult SuperAdmin()
        {
            return View();
        }
    }
}