﻿using DTO.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Interfaces.Rancheria
{
    public interface IRegistroCrecimiento
    {
        List<ClsRegistroCrecimiento> ListarCrecimiento();
        bool Borrar(ClsRegistroCrecimiento dto);
        bool Crear(ClsRegistroCrecimiento dto);
    }
}
