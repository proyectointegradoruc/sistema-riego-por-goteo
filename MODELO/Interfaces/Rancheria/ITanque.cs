﻿using DTO.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Interfaces.Rancheria
{
    public interface ITanque
    {
        bool Crear(ClsTanque dto);
        string GetTipo(int id);
        string GetCantidad(int id);
    }
}
