﻿using DTO.Rancheria;


using System;
using System.Collections.Generic;

namespace MODELO.Interfaces.Rancheria
{
    public interface IRancheria
    {
        bool Crear(ClsRancheria dto);

        bool Borrar(ClsRancheria dto);

        

        List<ClsRancheria> ListarRancheria();
    }
}