﻿using MODELO.Interfaces;
using MODELO.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using DTO.Seguridad;


namespace MODELO.Modelos
{
    public class ModUsuario : IUsuario
    {

        /// <summary>
        /// Metodo encargado de iniciar sesion y devolver la informacion del usuario
        /// </summary>
        /// <param name="dto">ClsUsuario con los datos para iniciar sesion</param>
        /// <returns>ClsUsuario si es valida la autenticaicon, null si no lo es</returns>
        public ClsUsuario IniciarSesion(ClsUsuario dto)
        {
            try
            {
                using (ESeguridad db = new ESeguridad())
                {
                    ClsUsuario aux = new ClsUsuario();
                    var usuario = db.Seg_Usuario.Where(u => u.USUARIO == dto.usuario && u.CONTRASENA == dto.contrasena);
                    if (usuario.Any())
                    {
                        var user = usuario.FirstOrDefault();
                        aux.id = user.ID;
                        aux.usuario = user.USUARIO;

                        return aux;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "IniciarSesion", "ModUsuario");
                return null;
            }
        }


        public string ListarMenus(ClsUsuario dto)
        {
            try
            {
                string mainMenu = "<ul class=\"nav navbar-nav\">";
                using (ESeguridad db = new ESeguridad())
                {
                    List<Seg_Menu> menu = db.Seg_Menu.Where(m => m.MENU_PADRE == null).ToList();

                    foreach (Seg_Menu m in menu)
                    {
                        var subMenu = db.Seg_Menu.Where(me => me.MENU_PADRE == m.ID);
                        mainMenu += "<li>";
                        if (subMenu.Any())
                        {
                            mainMenu += "<a href = \"/" + m.URL + "/\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">" + m.NOMBRE + "<b class=\"caret\"></b></a>";
                            mainMenu += "<ul class=\"dropdown-menu multi-level\">";

                            foreach (var sub in subMenu)
                            {
                                var tercerMenu = db.Seg_Menu.Where(t => t.MENU_PADRE == sub.ID);
                                if (tercerMenu.Any())
                                {
                                    mainMenu += "<li class=\"dropdown-submenu\">";
                                    mainMenu += "<a href =\"/" + sub.URL + "/\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">" + sub.NOMBRE + "</a>";
                                    mainMenu += "<ul class=\"dropdown-menu\">";
                                    foreach (var tercer in tercerMenu)
                                    {
                                        mainMenu += "<li><a href=\"/" + tercer.URL + "/\" >" + tercer.NOMBRE + "</a></li>";
                                    }
                                    mainMenu += "</ul>";
                                    mainMenu += "</li>";
                                }
                                else
                                {
                                    mainMenu += "<li><a href=\"/" + sub.URL + "/\" >" + sub.NOMBRE + "</a></li>";
                                }
                            }
                            mainMenu += "</ul>";
                            mainMenu += "</li>";
                        }
                        else
                        {
                            mainMenu += "<a href=\"/" + m.URL + "/\" >" + m.NOMBRE + "</a>";
                        }
                        mainMenu += "</li>";
                    }
                    mainMenu += "</ul>";
                }
                return mainMenu;
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ListarMenus", "ModUsuario");
                return null;
            }
        }

        /// <summary>
        /// Dado un usuario retorna una lista de roles del mismo
        /// </summary>
        /// <param name="dto">ClsUsario con el username para validar</param>
        /// <returns>Lista de string con los roles, null si el usuario no existe</returns>
        public List<string> ListarRoles(ClsUsuario dto)
        {
            try
            {
                using (ESeguridad db = new ESeguridad())
                {
                    return db.Seg_Rol_Usuario.Where(ru => ru.Seg_Usuario.USUARIO == dto.usuario).Select(ru => ru.Seg_Rol.NOMBRE).ToList();
                }
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ListarRoles", "ModUsuario");
                return null;
            }
        }

        public ClsUsuario ListarUsuario(string usuario)
        {
            throw new NotImplementedException();
        }

    }
}
