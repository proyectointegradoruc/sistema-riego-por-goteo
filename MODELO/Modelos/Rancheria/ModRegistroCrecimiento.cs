﻿using DTO.Rancheria;
using MODELO.Entidades;
using MODELO.Interfaces.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Modelos.Rancheria
{
    public class ModRegistroCrecimiento : IRegistroCrecimiento
    {
        public bool Borrar(ClsRegistroCrecimiento dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {

                    var crecimiento = db.Registro_Crecimiento.Where(r => r.Pk_Id == dto.id).FirstOrDefault();
                    db.Registro_Crecimiento.Remove(crecimiento);
                    db.SaveChanges();
                    return true;

                }
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModRegistroCrecimiento", "Borrar");
                return false;

            }
        }

        public bool Crear(ClsRegistroCrecimiento dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {
                    var existe = db.Registro_Crecimiento.Where(i => i.Pk_Id == dto.id);
                    if (existe.Any())
                    {
                        return false;
                    }
                    else
                    {
                        Registro_Crecimiento crecimiento = new Registro_Crecimiento();

                        crecimiento.Fecha = dto.fecha;
                        crecimiento.Fk_Planta = dto.fk_planta;
                        crecimiento.Valor = dto.valor;
                    
                        db.Registro_Crecimiento.Add(crecimiento);

                        db.SaveChanges();

                        return true;
                    }
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "Crear", "ModRegistroCrecimiento");
                return false;
            }
        }
        public List<ClsRegistroCrecimiento> ListarCrecimiento()
        {
            try
            {
                List<ClsRegistroCrecimiento> crecimientos = new List<ClsRegistroCrecimiento>();
                using (ERancheria db = new ERancheria())
                {
                    var lista = db.Registro_Crecimiento.ToList();

                    for (int i = 0; i < lista.Count; i++)
                    {
                        ClsRegistroCrecimiento crecimiento = new ClsRegistroCrecimiento();
                        crecimiento.id = lista[i].Pk_Id;
                        crecimiento.fk_planta = lista[i].Fk_Planta;
                        crecimiento.valor = lista[i].Valor;
                       
                   
                        crecimientos.Add(crecimiento);
                    }

                }

                return crecimientos;
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModRegistroCrecimiento", "ListarCrecimiento");
                return null;
            }
        }
    }
}
