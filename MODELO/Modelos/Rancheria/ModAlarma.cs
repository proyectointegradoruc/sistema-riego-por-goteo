﻿using DTO.Rancheria;
using MODELO.Entidades;
using MODELO.Interfaces.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Modelos.Rancheria
{
    public class ModAlarma : IAlarma
    {
        public bool Borrar(ClsAlarma dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {

                    var alarma = db.Alarma.Where(a => a.Pk_Id == dto.id).FirstOrDefault();
                    db.Alarma.Remove(alarma);
                    db.SaveChanges();
                    return true;

                }
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModAlarma", "Borrar");
                return false;

            }
        }

        public bool Crear(ClsAlarma dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {

                    Entidades.Alarma alarma = new Entidades.Alarma();

                    alarma.Fecha = dto.fecha;
                    alarma.Mensaje = dto.mensaje;
                    alarma.Fk_Cultivo = dto.cultivo;

                    db.Alarma.Add(alarma);
                    db.SaveChanges();

                    return true;
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "Crear", "ModAlarma");
                return false;
            }
        }

        public List<ClsAlarma> ListarAlarmas()
        {
            try
            {
                List<ClsAlarma> alarmas = new List<ClsAlarma>();
                using (ERancheria db = new ERancheria())
                {
                    var lista = db.Alarma.ToList();

                    for (int i = 0; i < lista.Count; i++)
                    {
                        ClsAlarma alarma = new ClsAlarma();

                        alarma.id = lista[i].Pk_Id;
                        alarma.fecha = lista[i].Fecha;
                        alarma.mensaje = lista[i].Mensaje;
                        alarma.cultivo = lista[i].Fk_Cultivo;


                        alarmas.Add(alarma);
                    }

                }

                return alarmas;
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModAlarma", "ListarAlarma");
                return null;
            }
        }
    }
}
