﻿using DTO.Rancheria;
using MODELO.Entidades;
using MODELO.Interfaces.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Modelos.Rancheria
{
    public class ModTanque : ITanque
    {

        public bool Crear(ClsTanque dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {
                    //debemos agregar tantos tanques como sea necesario para el cultivo
                    for (int i = 0; i < dto.cantidad_tanques; i++)
                    {

                        Tanque tanque = new Tanque();
                        tanque.Estado = "Activo";
                        tanque.Capacidad = dto.capacidad;
                        tanque.NivelActual = 100;
                        tanque.Bombeo = dto.bombeo;
                        db.Tanque.Add(tanque);

                        //Guardamos a que cultivo pertenece ese tanque
                        Cultivo_Tanque cultivo_tanque = new Cultivo_Tanque();
                        cultivo_tanque.Fk_Cultivo = dto.id_cultivo;
                        cultivo_tanque.Fk_Tanque = tanque.Pk_Id;
                        db.Cultivo_Tanque.Add(cultivo_tanque);
                        db.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModTanque", "Crear");
                return false;
            }
        }

        public string GetCantidad(int id)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {
                    var existe = db.Cultivo_Tanque.Where(i => i.Fk_Cultivo == id);
                    if (existe.Any())
                    {

                        return db.Cultivo_Tanque.Count(i => i.Fk_Cultivo == id) + "";
                    }
                    else
                    {



                        return null;
                    }
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "GetCantidad", "ModTanque");
                return null;

            }
        }
        public string GetTipo(int id)
        {

            try
            {
                using (ERancheria db = new ERancheria())
                {
                    var existe = db.Cultivo_Tanque.Where(i => i.Fk_Cultivo == id);
                    if (existe.Any())
                    {
                        var aux = db.Cultivo_Tanque.Where(i => i.Fk_Cultivo == id).FirstOrDefault();

                        return aux.Tanque.Bombeo;
                    }
                    else
                    {



                        return null;
                    }
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "GetCantidad", "ModTanque");
                return null;

            }
        }
    }

}
