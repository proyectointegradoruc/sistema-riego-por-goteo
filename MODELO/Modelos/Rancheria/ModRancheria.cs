﻿using DTO.Rancheria;
using MODELO.Entidades;
using MODELO.Interfaces.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Modelos.Rancheria
{
    public class ModRancheria : IRancheria
    {
        public bool Crear(ClsRancheria dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {

                    Entidades.Rancheria rancheria = new Entidades.Rancheria();

                    rancheria.Fk_Usuario = dto.usuario;
                    rancheria.Nombre = dto.nombre;
                    rancheria.Latitud = dto.latitud;
                    rancheria.Longitud = dto.longitud;

                    db.Rancheria.Add(rancheria);
                    db.SaveChanges();

                    return true;
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "Crear", "ModRancheria");
                return false;
            }

        }

        public List<ClsRancheria> ListarRancheria()
        {
            try
            {
                List<ClsRancheria> rancherias = new List<ClsRancheria>();
                using (ERancheria db = new ERancheria())
                {
                    var lista = db.Rancheria.ToList();

                    for (int i = 0; i < lista.Count; i++)
                    {
                        ClsRancheria rancheria = new ClsRancheria();
                        rancheria.id = lista[i].Pk_Id;
                        rancheria.nombre = lista[i].Nombre;
                        rancheria.usuario = lista[i].Fk_Usuario;
                        rancheria.latitud = lista[i].Latitud;
                        rancheria.longitud = lista[i].Longitud;
                        rancherias.Add(rancheria);
                    }

                }

                return rancherias;
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModRancheria", "ListarRancheria");
                return null;
            }
        }

        public bool Borrar(ClsRancheria dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {

                    var rancheria = db.Rancheria.Where(r => r.Pk_Id == dto.id).FirstOrDefault();
    
                    db.Rancheria.Remove(rancheria);
                  
                    db.SaveChanges();
                    
                    return true;

                }
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModRancheria", "Borrar");
                return false;

            }
        }

       

    }
}
