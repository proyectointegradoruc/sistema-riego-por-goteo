﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Rancheria
{
    public class ClsCultivo
    {
        [Key]
        public int id { set; get; }

        [Required]
        [Display(Name = "Rancheria")]
        public int idRaancheria { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de siembra")]
        public DateTime fechaSiembra { set; get; }

        [Required]
        [Display(Name = "Ancho")]
        public int ancho { get; set; }

        [Required]
        [Display(Name = "Alto")]
        public int alto { get; set; }
        [Display(Name = "Nombre Rancheria")]
        public string nombre_rancheria { get; set; }

        public string Estado { get; set; }

    }
}
