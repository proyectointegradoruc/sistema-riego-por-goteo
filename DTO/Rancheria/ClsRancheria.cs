﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Rancheria
{
    public class ClsRancheria
    {
        [Key]
        public int id { set; get; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Nombre")]
        public string nombre { set; get; }


        [DataType(DataType.Text)]
        [Display(Name = "Usuario")]
        public int usuario { set; get; }

        [Required]
        [Display(Name = "Latitud")]
        public string latitud { get; set; }
        [Required]
        [Display(Name = "Longitud")]
        public string longitud { get; set; }

    }
}
