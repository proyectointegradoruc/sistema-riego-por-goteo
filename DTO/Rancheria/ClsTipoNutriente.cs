﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Rancheria
{
    public class ClsTipoNutriente
    {
        [Key]
        public int id { set; get; }

        [Required]
        [Display(Name = "Nombre")]
        public string nombre { get; set; }

        [Required]
        [Display(Name = "Cantidad (kg)")]
        public int cantidad { set; get; }

        [Required]
        [Display(Name = "Rancheria")]
        public int rancheria { get; set; }
    }
}
