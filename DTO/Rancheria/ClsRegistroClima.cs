﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Rancheria
{
    public class ClsRegistroClima
    {
        [Key]
        public int id { set; get; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de registro clima")]
        public DateTime fecha { set; get; }
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "registro")]

        public decimal registro { set; get; }
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Cultivo")]
        public int fk_cultivo { set; get; }
    }
}
